<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'category_id' => $faker->numberBetween(1,3),
        'brand_id' => $faker->numberBetween(1,3),
        'remote_id' => $faker->unique()->numberBetween(1,3000),
        'name' => ($name = $faker->word),
        'slug' => str_slug($name),
        'description' => $faker->paragraph,
        'price_usd' => $faker->randomFloat(2, 0, 1000),
        'icon_path' => $faker->image($dir = './public/images/products', $width = 200, $height = 200),
        'model_path' => $faker->image($dir = './public/models', $width = 200, $height = 200),
        'sku' => $faker->numberBetween(10000000,99999999),
    ];
});
