<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductRequest as StoreRequest;
use App\Http\Requests\ProductRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('product', 'products');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $fields = [
            [
                'name' => 'remote_id',
                'label' => 'Remote ID',
                'type' => 'number',
            ],
            [
                'label' => "Category",
                'type' => 'select',
                'name' => 'category_id',
                'entity' => 'category',
                'attribute' => 'name',
                'model' => "App\Models\Category"
            ],
            [
                'label' => "Brand",
                'type' => 'select',
                'name' => 'brand_id',
                'entity' => 'brand',
                'attribute' => 'name',
                'model' => "App\Models\Brand"
            ],
            ['name' => 'name', 'label' => "Name", 'type' => 'text'],
            ['name' => 'slug', 'label' => "Slug", 'type' => 'text'],
            ['name' => 'description', 'label' => "Description", 'type' => 'textarea'],
            ['name' => 'price_usd', 'label' => 'Price', 'type' => 'number', 'prefix' => "$", 'attributes' => ["step" => "0.01"]],
            ['name' => 'icon_path', 'label' => "Path to icon", 'type' => 'text'],
            ['name' => 'model_path', 'label' => "Path for unity model", 'type' => 'text'],
            ['name' => 'sku', 'label' => 'SKU', 'type' => 'number'],
            ['name' => 'created_at', 'type' => 'text', 'label' => 'Created at', 'attributes' => ['readonly' => 'readonly']],
            ['name' => 'updated_at', 'type' => 'text', 'label' => 'Updated at', 'attributes' => ['readonly' => 'readonly']],
        ];

        foreach ($fields as $field) {
            $this->crud->addField($field);
        }

        $columns = [
            ['name' => 'id', 'label' => "ID", 'type' => 'number'],
            ['name' => 'remote_id', 'label' => 'Remote ID', 'type' => 'number',],
            [
                'label' => "Category",
                'type' => 'select',
                'name' => 'category_id',
                'entity' => 'category',
                'attribute' => 'name',
                'model' => "App\Models\Category"
            ],
            [
                'label' => "Brand",
                'type' => 'select',
                'name' => 'brand_id',
                'entity' => 'brand',
                'attribute' => 'name',
                'model' => "App\Models\Brand"
            ],
            ['name' => 'sku', 'label' => 'SKU', 'type' => 'number'],
            ['name' => 'name', 'label' => "Name", 'type' => 'text']
        ];

        foreach ($columns as $column) {
            $this->crud->addField($column);
            $this->crud->addColumn($column);
        }


        // add asterisk for fields that are required in ProductRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        if (is_null($request->get('slug'))) {
            $request->merge([
                'slug' => str_slug($request->get('name')),
            ]);
        }

        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        if (is_null($request->get('slug'))) {
            $request->merge([
                'slug' => str_slug($request->get('name')),
            ]);
        }

        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
