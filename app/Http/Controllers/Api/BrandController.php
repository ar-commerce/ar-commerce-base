<?php

namespace App\Http\Controllers\Api;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    /**
     * Get all brands
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['message' => 'ok', 'brands' => Brand::all()]);
    }

    /**
     * Get brand by filters
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        if(empty($request->all())) {
            return response()->json(['error' => "Set at least one filter parameter"], 400);
        }

        if($request->has('limit')) {
            $limit = $request->get('limit');
            $brands = Brand::where($request->except('limit'))->get()->take($limit);
            return response()->json(['message' => 'ok', 'brands' => $brands]);
        }

        if($request->has('page')) {
            $page = $request->get('page');
            $perpage = $request->get('perpage') ?: 2;
            $brands = Brand::where($request->except('page', 'perpage'))->get()->forPage($page, $perpage);
            return response()->json(['message' => 'ok', 'brands' => $brands]);
        }

        $brands = Brand::where($request->all())->get();

        return response()->json(['message' => 'ok', 'brands' => $brands]);
    }
}
