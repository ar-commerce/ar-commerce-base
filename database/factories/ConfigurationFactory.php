<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Configuration::class, function (Faker $faker) {
    $randomData = [];
    for ($i = 0; $i < 3; $i++) {
        $randomID = $faker->numberBetween(1,3);
        $randomProduct = \App\Models\Product::where('id', $randomID)->get()->first();
        $randomData[] = [
            'category_id' => $randomProduct->category_id,
            'product_id' => $randomProduct->id,
        ];
    }

    return [
        'user_id' => $faker->numberBetween(1,3),
        'data' => serialize($randomData),
    ];
});