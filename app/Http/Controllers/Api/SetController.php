<?php

namespace App\Http\Controllers\Api;

use App\Models\Set;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SetController extends Controller
{
    /**
     * Get all sets
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['message' => 'ok', 'sets' => Set::all()]);
    }

    /**
     * Get set by filters
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        if(empty($request->all())) {
            return response()->json(['error' => "Set at least one filter parameter"], 400);
        }

        if($request->has('limit')) {
            $limit = $request->get('limit');
            $sets = Set::where($request->except('limit'))->get()->take($limit);
            return response()->json(['message' => 'ok', 'sets' => $sets]);
        }

        if($request->has('page')) {
            $page = $request->get('page');
            $perpage = $request->get('perpage') ?: 2;
            $sets = Set::where($request->except('page', 'perpage'))->get()->forPage($page, $perpage);
            return response()->json(['message' => 'ok', 'sets' => $sets]);
        }

        $sets = Set::where($request->all())->get();

        return response()->json(['message' => 'ok', 'sets' => $sets]);
    }
}
