<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Brand::class, function (Faker $faker) {
    return [
        'name' => ($name = $faker->company),
        'slug' => str_slug($name),
        'icon_path' => $faker->image($dir = './public/images/brands', $width = 200, $height = 200),
    ];
});
