<?php

use App\Models\{Category, Product, ProductPhoto};
use Illuminate\Database\Seeder;

class CustomDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customData = ["chair_01", "chair_02", "chair_03", "chair_04", "chair_05", "chair_06", "chair_07", "chair_08", "chair_09", "chair_10",
            "chair_11", "chair_12", "chair_13", "chair_14", "chair_15", "chair_16", "chair_17", "chair_18", "chair_19", "chair_20",
            "chair_21", "chair_22", "chair_23", "chair_24", "chair_25", "chair_26", "chair_27", "chair_28", "chair_29", "chair_30",
            "chair_31", "chair_32", "chair_33", "chair_34", "lamp_01", "sofa_01", "sofa_02", "sofa_03", "sofa_04", "sofa_05",
            "sofa_06", "sofa_07", "sofa_08", "sofa_09", "sofa_10", "sofa_11", "sofa_12", "sofa_13", "sofa_14", "sofa_15",
            "sofa_16", "sofa_17", "sofa_18", "stand_01", "stand_02", "stand_03", "stand_04", "table_01", "table_02", "table_03",
            "table_04", "table_05", "table_06", "table_07", "table_08", "table_10", "table_11", "table_12", "table_13",
            "table_09", "vase"];

        $lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus viverra mollis turpis euismod
        finibus. Morbi vel scelerisque neque. Donec sollicitudin, metus ut ultrices laoreet, libero ligula pharetra ex,
        quis gravida est leo eu lacus. Nam at justo sodales, accumsan ex ac, gravida augue. Donec facilisis ex consequat
        leo convallis, nec dapibus odio fermentum. Phasellus blandit leo purus, ac posuere diam laoreet ut. Morbi in
        pharetra purus, in rhoncus sapien. Phasellus aliquet felis ligula, sed porttitor turpis pretium non.";

        foreach ($customData as $index => $item) {
            $category = $this->findOrCreateCategory($item);

            Product::create([
                "remote_id" => $index,
                "category_id" => $category->id,
                "icon_path" => null,
                "name" => $item,
                "slug" => str_slug($item),
                "price_usd" => rand(50, 1000),
                "description" => $lorem,
                "model_path" => "models/${item}"
            ]);

            ProductPhoto::create([
                'product_id' => $index + 1,
                'filename' => "${item}_icon.png"
            ]);

            foreach (['r', 'f'] as $side) {
                ProductPhoto::create([
                    'product_id' => $index + 1,
                    'filename' => "${item}${side}_icon.png"
                ]);
            }
        }
    }

    public function findOrCreateCategory($item)
    {
        $categoryName = $item;
        if (strpos($item, '_') !== false) {
            preg_match('/(.*?)\_/m', $item, $matches);
            $categoryName = $matches[1];
        }

        $category = Category::firstOrCreate(
            ['name' => $categoryName],
            [
                'name' => $categoryName,
                'slug' => str_slug($categoryName),
                'icon_path' => str_slug($categoryName) . "_category_icon.png"
            ]);

        return $category;
    }
}
