<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ColorOptionRequest as StoreRequest;
use App\Http\Requests\ColorOptionRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ColorOptionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ColorOptionCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ColorOption');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/coloroption');
        $this->crud->setEntityNameStrings('coloroption', 'color_options');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $columns = [
            ['name' => 'id', 'label' => "ID", 'type' => 'number'],
            ['name' => 'name', 'label' => 'Name', 'type' => 'text'],
            ['name' => 'icon_path', 'label' => 'Path to icon', 'type' => 'text'],
            ['name' => 'color', 'label' => 'Color', 'type' => 'text'],
            [
                'label' => "Product",
                'type' => 'select',
                'name' => 'product_id',
                'entity' => 'product',
                'attribute' => 'name',
                'model' => "App\Models\Product"
            ],
        ];

        foreach ($columns as $column) {
            $this->crud->addField($column);
            $this->crud->addColumn($column);
        }

        // add asterisk for fields that are required in ColorOptionRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
