<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ColorOption::class, function (Faker $faker) {
    return [
        'name' => ($name = $faker->colorName),
        'slug' => str_slug($name),
        'icon_path' => $faker->image($dir = './public/images/color_options', $width = 200, $height = 200),
    ];
});
