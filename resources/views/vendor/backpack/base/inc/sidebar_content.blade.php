<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li><a href='{{ backpack_url('product') }}'><i class='fa fa-tag'></i> <span>Products</span></a></li>
<li><a href='{{ backpack_url('brand') }}'><i class='fa fa-tag'></i> <span>Brands</span></a></li>
<li><a href='{{ backpack_url('category') }}'><i class='fa fa-tag'></i> <span>Categories</span></a></li>
<li><a href='{{ backpack_url('coloroption') }}'><i class='fa fa-tag'></i> <span>Color Options</span></a></li>
<li><a href='{{ backpack_url('configuration') }}'><i class='fa fa-tag'></i> <span>Configurations</span></a></li>
<li><a href='{{ backpack_url('favorites') }}'><i class='fa fa-tag'></i> <span>Favorites</span></a></li>
<li><a href='{{ backpack_url('productphoto') }}'><i class='fa fa-tag'></i> <span>Product Photos</span></a></li>
<li><a href='{{ backpack_url('set') }}'><i class='fa fa-tag'></i> <span>Sets</span></a></li>
<li><a href='{{ backpack_url('user') }}'><i class='fa fa-tag'></i> <span>Users</span></a></li>