<?php

use Illuminate\Database\Seeder;

class BaseDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 3)->create();
        factory(App\Models\Brand::class, 3)->create();
        factory(App\Models\Category::class, 3)->create();
        factory(App\Models\ColorOption::class, 3)->create();
        factory(App\Models\Product::class, 20)->create();
        factory(App\Models\Configuration::class, 3)->create();
        factory(App\Models\Set::class, 3)->create();
        factory(App\Models\Favorites::class, 3)->create();
    }
}
