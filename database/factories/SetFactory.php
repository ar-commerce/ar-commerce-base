<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Set::class, function (Faker $faker) {
    $randomData = [];
    for ($i = 0; $i < 3; $i++) {
        $randomID = $faker->numberBetween(1,3);
        $randomProduct = \App\Models\Product::where('id', $randomID)->get()->first();
        $randomData[] = [
            'category_id' => $randomProduct->category_id,
            'product_id' => $randomProduct->id,
        ];
    }

    return [
        'name' => ($name = $faker->company),
        'slug' => str_slug($name),
        'icon_path' => $faker->image($dir = './public/images/sets', $width = 200, $height = 200),
        'data' => $randomData,
    ];
});
