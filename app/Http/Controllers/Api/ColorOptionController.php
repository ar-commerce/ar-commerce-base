<?php

namespace App\Http\Controllers\Api;

use App\Models\ColorOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorOptionController extends Controller
{
    /**
     * Get all color options
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['message' => 'ok', 'coloroptions' => ColorOption::all()]);
    }

    /**
     * Get color option by filters
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        if(empty($request->all())) {
            return response()->json(['error' => "Set at least one filter parameter"], 400);
        }

        if($request->has('limit')) {
            $limit = $request->get('limit');
            $coloroptions = ColorOption::where($request->except('limit'))->get()->take($limit);
            return response()->json(['message' => 'ok', 'coloroptions' => $coloroptions]);
        }

        if($request->has('page')) {
            $page = $request->get('page');
            $perpage = $request->get('perpage') ?: 2;
            $coloroptions = ColorOption::where($request->except('page', 'perpage'))->get()->forPage($page, $perpage);
            return response()->json(['message' => 'ok', 'coloroptions' => $coloroptions]);
        }

        $coloroptions = ColorOption::where($request->all())->get();

        return response()->json(['message' => 'ok', 'coloroptions' => $coloroptions]);
    }
}
