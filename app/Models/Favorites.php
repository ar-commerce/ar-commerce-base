<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Favorites extends Model
{
    use CrudTrait;

    public $timestamps = false;
}
