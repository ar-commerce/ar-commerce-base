<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductPhotoRequest as StoreRequest;
use App\Http\Requests\ProductPhotoRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ProductPhotoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductPhotoCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ProductPhoto');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/productphoto');
        $this->crud->setEntityNameStrings('productphoto', 'product_photos');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $fields = [
            ['name' => 'id', 'label' => "Photo ID", 'type' => 'number', 'attributes' => ['readonly' => 'readonly']],
            [
                'label' => "Product",
                'type' => 'select',
                'name' => 'product_id',
                'entity' => 'product',
                'attribute' => 'name',
                'model' => "App\Models\Product"
            ],
//            ['name' => 'filename', 'label' => "Filename", 'type' => 'text'],
            [
                'label' => "Product Image",
                'name' => "filename",
                'type' => 'image',
                'upload' => true,
                'prefix' => 'images/products/photo/' // in case you only store the filename in the database, this text will be prepended to the database value
            ]
        ];

        foreach ($fields as $field) {
            $this->crud->addField($field);
            $this->crud->addColumn($field);
        }

        // add asterisk for fields that are required in ProductPhotoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
