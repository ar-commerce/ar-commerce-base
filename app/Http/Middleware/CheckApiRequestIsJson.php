<?php

namespace App\Http\Middleware;

use Closure;

class CheckApiRequestIsJson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isJson() || $request->wantsJson()) {
            return $next($request);
        }

        return redirect('/');
    }
}
