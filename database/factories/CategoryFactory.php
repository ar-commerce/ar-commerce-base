<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    return [
        'parent_category_id' => null,
        'name' => ($name = $faker->company),
        'slug' => str_slug($name),
        'icon_path' => $faker->image($dir = './public/images/categories', $width = 200, $height = 200),
    ];
});
