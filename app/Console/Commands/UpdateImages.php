<?php

namespace App\Console\Commands;

use App\Models\Brand;
use App\Models\Category;
use App\Models\ColorOption;
use App\Models\Product;
use App\Models\Set;
use Illuminate\Console\Command;

class UpdateImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update updated_at for changes images';

    protected $imageCategory;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = $this->getFiles();

        $currentDate = date("Y-m-d", now()->getTimestamp());

        $changedFiles = $this->getFilesByDate($files, $currentDate);

        $countOfChangesFiles = $this->updateItems($changedFiles);

        $this->info("Updated ${countOfChangesFiles} items for type " . $this->imageCategory);
    }

    public function getFiles()
    {
        $this->imageCategory = $imageCategory = $this->anticipate('What images you changed?',
            ['products', 'categories', 'brands', 'color_options', 'sets'], 'products');

        $type = $this->anticipate('Icons or images?', ['icons', 'images'], 'icons');

        $path = $type === 'icons'
            ? public_path("/images/${imageCategory}/icons/*.*")
            : public_path("/images/${imageCategory}/*.*");

        return glob($path, GLOB_ERR);
    }

    public function getFilesByDate($files, $date, $format = "Y-m-d")
    {
        $selectedFiles = [];

        foreach ($files as $file) {
            if (date ($format, filemtime ($file)) == $date) {
                $selectedFiles[] = basename($file);
            }
        }

        return $selectedFiles;
    }

    public function updateItems($filenamesList)
    {
        $countOfChangesFiles = 0;
        $model = $this->getModelClass();
        foreach ($filenamesList as $filename) {
            $model->where('icon_path', $filename)->update(['icon_path' => $filename]);
            $countOfChangesFiles++;
        }

        return $countOfChangesFiles;
    }

    public function getModelClass()
    {
        $typesAndModels = [
            'products' => new Product(),
            'categories' => new Category(),
            'brands' => new Brand(),
            'color_options' => new ColorOption(),
            'sets' => new Set()
        ];

        return $typesAndModels[$this->imageCategory];
    }
}
