[Base API Postman](https://elementalsweb.postman.co/collections/4147788-41b85437-1c04-4dc2-a29e-191582e5df6c?workspace=2eaa33f9-67b0-4bfc-8148-d9444e39ca25#)

## Installation

```bash
git clone git@gitlab.com:ar-commerce/ar-commerce-base.git
cd ar-commerce-base
composer install
```

-   Create new DB

```bash
cp .env.example .env
php artisan key:generate
```

-   Setting up env
-   Setting up server config
