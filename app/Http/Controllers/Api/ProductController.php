<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Get all products
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['message' => 'ok', 'products' => Product::with(['coloroption'])->withModel()->withPhoto()->get()]);
    }

    /**
     * Get product by filters
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        if (empty($request->all())) {
            return response()->json(['error' => "Set at least one filter parameter"], 400);
        }

        if ($request->has('limit')) {
            $limit = $request->get('limit');
            $products = Product::where($request->except('limit'))->withModel()->withPhoto()->get()->take($limit);
            return response()->json(['message' => 'ok', 'products' => $products]);
        }

        if ($request->has('page')) {
            $page = $request->get('page');
            $perpage = $request->get('perpage') ?: 2;
            $products = Product::where($request->except('page', 'perpage'))->withModel()->withPhoto()->get()->forPage($page, $perpage);
            return response()->json(['message' => 'ok', 'products' => $products]);
        }

        $products = Product::where($request->all())->withModel()->withPhoto()->get();

        return response()->json(['message' => 'ok', 'products' => $products]);
    }
}
