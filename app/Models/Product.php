<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Product extends Model
{
    use CrudTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'remote_id', 'name', 'slug', 'description', 'price_usd', 'icon_path', 'model_path', 'sku'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'price_usd' => 'float'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function coloroption()
    {
        return $this->hasMany(ColorOption::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function photo()
    {
        return $this->hasMany(ProductPhoto::class);
    }

    public function scopeWithModel($query)
    {
        return $query->whereNotNull('model_path');
    }

    public function scopeWithPhoto($query)
    {
        return $query->with('photo');
    }
}
