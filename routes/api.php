<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'prefix' => 'v1'], function () {
    Route::post('register', 'AuthController@register')->name('api.register');
    Route::post('login', 'AuthController@login')->name('api.login');

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'ProductController@index')->name('api.product.all');
        Route::post('filter', 'ProductController@filter')->name('api.product.filter');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'CategoryController@index')->name('api.category.all');
        Route::post('filter', 'CategoryController@filter')->name('api.category.filter');
    });

    Route::group(['prefix' => 'coloroption'], function () {
        Route::get('/', 'ColorOptionController@index')->name('api.coloroption.all');
        Route::post('filter', 'ColorOptionController@filter')->name('api.coloroption.filter');
    });

    Route::group(['prefix' => 'brand'], function () {
        Route::get('/', 'BrandController@index')->name('api.brand.all');
        Route::post('filter', 'BrandController@filter')->name('api.brand.filter');
    });

    Route::group(['prefix' => 'set'], function () {
        Route::get('/', 'SetController@index')->name('api.set.all');
        Route::post('filter', 'SetController@filter')->name('api.set.filter');
    });
});

Route::group(['middleware' => 'auth:api', 'namespace' => 'Api', 'prefix' => 'v1'], function(){
    Route::post('logout', 'AuthController@logout')->name('api.logout');

    Route::group(['prefix' => 'configuration'], function () {
        Route::get('/', 'ConfigurationController@index')->name('api.configuration.all');
        Route::post('store', 'ConfigurationController@store')->name('api.configuration.store');
        Route::post('filter', 'ConfigurationController@filter')->name('api.configuration.filter');
    });

});