<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Favorites::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1,3),
        'product_id' => $faker->numberBetween(1,3),
    ];
});
