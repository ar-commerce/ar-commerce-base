<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return response()->json(['message' => 'ok', 'categories' => Category::withCount('products')->get()]);
    }

    /**
     * Get category by filters
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        if(empty($request->all())) {
            return response()->json(['error' => "Set at least one filter parameter"], 400);
        }

        if ($request->has('orderBy') && $request->has('orderDirection')) {
            $sortedValue = $request->get('orderBy');
            $sortedDirection = $request->get('orderDirection');
            $categories = Category::orderBy($sortedValue, $sortedDirection)->withCount('products')->get();

            return response()->json([
                'message' => 'ok',
                'orderBy' => $sortedValue,
                'orderDirection' => $sortedDirection,
                'categories' => $categories
            ]);
        }

        if($request->has('limit')) {
            $limit = $request->get('limit');
            $categories = Category::where($request->except('limit'))->get()->take($limit);
            return response()->json(['message' => 'ok', 'limit' => $limit, 'categories' => $categories]);
        }

        if($request->has('page')) {
            $page = $request->get('page');
            $perpage = $request->get('perpage') ?: 2;
            $categories = Category::where($request->except('page', 'perpage'))->get()->forPage($page, $perpage);
            return response()->json(['message' => 'ok', 'categories' => $categories]);
        }

        $categories = Category::where($request->all())->get();

        return response()->json(['message' => 'ok', 'categories' => $categories]);
    }
}
