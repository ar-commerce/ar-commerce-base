<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Lcobucci\JWT\Parser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Api\Auth\RegisterRequest;

class AuthController extends Controller
{
    /**
     * Login user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $success['token'] = $user->createToken('ARC')->accessToken;

            return response()->json($success, 200);
        }

        return response()->json(['error' => "Email or password is incorrect"], 401);
    }

    /**
     * Register user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $data = $request->all();

        if(User::where('email', $data['email'])->exists()) {
            return response()->json(['message' => 'Looks like this email is already registered'], 401);
        }

        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);

        $success['token'] = $user->createToken('ARC')->accessToken;

        return response()->json($success, 200);
    }

    /**
     * Logout user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function logout(Request $request) {
        Auth::user()->token()->revoke();

        return response()->json(['message' => 'You are successfully logged out'], 200);
    }
}
