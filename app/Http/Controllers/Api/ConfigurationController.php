<?php

namespace App\Http\Controllers\Api;

use App\Models\Configuration;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigurationController extends Controller
{
    /**
     * Get all configurations options
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return response()->json(['message' => 'ok', 'configurations' => Configuration::all()]);
    }

    /**
     * Get configuration by filters
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        if(empty($request->all())) {
            return response()->json(['error' => "Set at least one filter parameter"], 400);
        }

        if($request->has('limit')) {
            $limit = $request->get('limit');
            $configurations = Configuration::where($request->except('limit'))->get()->take($limit);
            return response()->json(['message' => 'ok', 'configurations' => $configurations]);
        }

        if($request->has('page')) {
            $page = $request->get('page');
            $perpage = $request->get('perpage') ?: 2;
            $configurations = Configuration::where($request->except('page', 'perpage'))->get()->forPage($page, $perpage);
            return response()->json(['message' => 'ok', 'configurations' => $configurations]);
        }

        $configurations = Configuration::where($request->all())->get();

        return response()->json(['message' => 'ok', 'configurations' => $configurations]);
    }

    /**
     * Store new configuration
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if(!$request->has('user_id')) {
            return response()->json(['error' => "Whoops! user_id is missing"], 400);
        }

        if (User::where('id', $request->user_id)->first() === null) {
            return response()->json(['error' => "Whoops! Looks like user with this ID does not exist"], 400);
        }

        if(!$request->has('data')) {
            return response()->json(['error' => "Whoops! Configuration data is missing"], 400);
        }

        $newConfiguration = Configuration::create($request->all());

        return response()->json(['message' => 'ok', 'configuration' => $newConfiguration]);
    }
}
