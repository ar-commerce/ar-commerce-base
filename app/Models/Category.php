<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Category extends Model
{
    use CrudTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'slug', 'icon_path'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
        'icon_path' => 'string',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function setIconPathAttribute($value)
    {
        $attribute_name = "icon_path";
        $disk = "images";
        $destination_path = "categories/icons";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($destination_path.'/'.$this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            \Storage::disk($disk)->delete($destination_path.'/'.$this->{$attribute_name});
            $image = \Image::make($value);
            $filename = md5($value.time()).'.png';
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            $this->attributes[$attribute_name] = $filename;
        }
    }
}
