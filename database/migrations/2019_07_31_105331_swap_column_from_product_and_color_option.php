<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SwapColumnFromProductAndColorOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('coloroption_id');
        });

        Schema::table('color_options', function (Blueprint $table) {
            $table->integer('product_id');
            $table->string('color')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('color_options', function (Blueprint $table) {
            $table->dropColumn('product_id');
            $table->dropColumn('color');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->integer('coloroption_id');
        });
    }
}
